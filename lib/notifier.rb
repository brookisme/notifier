module Notifier
  def self.config
    @config ||= YAML.load(File.read(File.expand_path('config/notifier.yml',Rails.root)))
  end
end
require 'notifier/models/concerns/notifiable.rb'
require 'notifier/models/concerns/notify_listenable.rb'
require 'notifier/models/notifier_listener.rb'

