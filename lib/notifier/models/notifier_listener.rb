class NotifierListener < ActiveRecord::Base
  belongs_to :notifier, class_name: Notifier.config['notifier_class']
  belongs_to :listener, class_name: Notifier.config['listener_class']
end
