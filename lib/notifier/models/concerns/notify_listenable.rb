module NotifyListenable
extend ActiveSupport::Concern
  included do
    has_many :notifier_listeners, foreign_key: "notifier_id"
    has_many :listeners, class_name: Notifier.config['listener_class'], through: :notifier_listeners
  end
end
