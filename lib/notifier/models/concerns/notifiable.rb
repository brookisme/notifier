module Notifiable
extend ActiveSupport::Concern

  #
  #  Expects:
  #  bools - send_email, send_sms
  #

  included do
    has_many :notifier_listeners, class_name: "NotifierListener", foreign_key: "listener_id"
    has_many :notifiers, class_name: "SurveyBase::Group", through: :notifier_listeners
  end

  module ClassMethods
    def publish_event event_name, listener, sender, notifier, found_filter
      ActiveSupport::Notifications.instrument("notifiable.#{event_name}", data: {
        listener_id: listener.id,
        listener_class: listener.class.to_s,
        sender_id: sender.id,
        sender_class: sender.class.to_s,
        notifier_id: notifier.id,
        notifier_class: notifier.class.to_s,
        found_filter: found_filter
      })
    end
  end

  def notify sender, notifier, found_filter
    self.class.publish_event("email",self,sender,notifier,found_filter) if send_email
    self.class.publish_event("sms",self,sender,notifier,found_filter) if send_sms
  end
end
