$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "notifier/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "notifier"
  s.version     = Notifier::VERSION
  s.authors     = ["Brook"]
  s.email       = ["brookwilliams@laborvoices.com"]
  s.homepage    = "http://laborvoices.com"
  s.summary     = "Notifier Machine"
  s.description = "Notifier Machine"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.1.0"

  s.add_development_dependency "rspec-rails"
end
